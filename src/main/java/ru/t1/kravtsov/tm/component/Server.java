package ru.t1.kravtsov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.endpoint.IOperation;
import ru.t1.kravtsov.tm.api.service.IPropertyService;
import ru.t1.kravtsov.tm.api.service.IServiceLocator;
import ru.t1.kravtsov.tm.dto.request.AbstractRequest;
import ru.t1.kravtsov.tm.dto.response.AbstractResponse;
import ru.t1.kravtsov.tm.task.AbstractServiceTask;
import ru.t1.kravtsov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    @NotNull
    private final IServiceLocator serviceLocator;

    public Server(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void submit(@NotNull final AbstractServiceTask task) {
        executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final IOperation<RQ, RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        executorService.shutdown();
        serverSocket.close();
    }

}
