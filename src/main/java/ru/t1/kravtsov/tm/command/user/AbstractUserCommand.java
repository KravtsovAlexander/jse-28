package ru.t1.kravtsov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.service.IUserService;
import ru.t1.kravtsov.tm.command.AbstractCommand;
import ru.t1.kravtsov.tm.exception.entity.UserNotFoundException;
import ru.t1.kravtsov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    protected void displayUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
